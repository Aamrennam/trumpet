package com.therynnings.trumpet;

import org.sikuli.android.ADBDevice;
import org.sikuli.android.ADBRobot;
import org.sikuli.android.ADBScreen;
import org.sikuli.script.FindFailed;
import org.sikuli.script.ScreenImage;
import org.sikuli.script.TextRecognizer;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestingStuff {
    public static void main(String[] args) {
        TestingStuff testingStuff = new TestingStuff();
        //testingStuff.getArmyMarchTime();

        Trumpet screen = new Trumpet();
        screen.tap(1014, 1506);
    }

    public void getArmyMarchTime() {
        HashMap<String, Rectangle> gamePositions = new HashMap<String, Rectangle>();
        gamePositions.put("ArmyMarchTime1", new Rectangle(117, 354, 101, 28));
        gamePositions.put("ArmyMarchTime2", new Rectangle(117, 445, 101, 28));
        gamePositions.put("ArmyMarchTime3", new Rectangle(117, 530, 101, 28));
        gamePositions.put("ArmyMarchTime4", new Rectangle(117, 615, 101, 28));
        gamePositions.put("ArmyMarchTime5", new Rectangle(117, 695, 101, 28));

        TextRecognizer textRecognizer = TextRecognizer.getInstance();
        Rectangle m;
        BufferedImage cropMarch;
        ScreenImage gameImage;
        String result;

        BufferedImage image;
        try {
            image = ImageIO.read(new File("C:\\temp\\sikuliximage-1502298850173.png"));
            for (Map.Entry entry : gamePositions.entrySet()) {
                m = (Rectangle) entry.getValue();
                String key = (String) entry.getKey();
                cropMarch = image.getSubimage(m.x, m.y, m.width, m.height);
                gameImage = new ScreenImage(new Rectangle(), cropMarch);
                result = textRecognizer.recognizeWord(gameImage).replaceAll("\\s", "");
                System.out.println(key + ": " + result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
