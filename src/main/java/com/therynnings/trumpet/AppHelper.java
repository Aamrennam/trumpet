package com.therynnings.trumpet;

import org.sikuli.android.ADBScreen;
import org.sikuli.basics.Debug;
import org.sikuli.script.RunTime;
import org.sikuli.script.ScreenImage;

import java.io.IOException;
/*
This class helps taking photos from screen.
 */
public class AppHelper {
    static {
        RunTime.loadLibrary("VisionProxy");
        RunTime.loadLibrary("libopencv_core248");
        RunTime.loadLibrary("libopencv_java248");
    }

    public static void main(String[] args) throws IOException {
        Debug.setDebugLevel(3);
        System.out.println(Debug.getDebugLevel());

        ADBScreen screen = ADBScreen.start();

        while (true) {
            System.out.println("Press enter to take screenshot");
            System.in.read();
            ScreenImage image = screen.capture();
            String filename = image.save("C:\\Temp");
            // Open photo with mspaint.
            //Runtime.getRuntime().exec("C:\\WINDOWS\\system32\\mspaint.exe "+ filename);
        }
    }
}
