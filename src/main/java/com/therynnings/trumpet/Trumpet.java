package com.therynnings.trumpet;

import org.sikuli.android.ADBScreen;
import org.sikuli.basics.Debug;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.RunTime;

public class Trumpet extends ADBScreen {
    ADBScreen screen;

    static {
        RunTime.loadLibrary("VisionProxy");
        RunTime.loadLibrary("libopencv_core248");
        RunTime.loadLibrary("libopencv_java248");
    }

    public Trumpet() {
        screen = ADBScreen.start();
        screen.setObserveScanRate(1F);
        screen.delayClick(200);
        screen.delayType(700);
        screen.setOtherScreen();
        screen.setAutoWaitTimeout(30);
        Debug.info("Created trumpet instance. Debug log level = " + Integer.toString(Debug.getDebugLevel()));
    }

    public void tap(int x, int y) {
        try {
            screen.aTap(screen.newLocation(x, y));
        } catch (FindFailed findFailed) {
            Debug.error(findFailed.getMessage());
        }
    }

    public Match waitClick(String target) {
        return this.waitClick(target, 30);
    }

    public Match waitClick(String target, double timeout) {
        try {
            Match match = screen.wait(target, timeout);
            screen.aTap(match);
            return match;
        } catch (FindFailed findFailed) {
            Debug.error(findFailed.getMessage());
        }
        return null;
    }

    public Match find(String target, double score) {
        try {
            Match match = screen.find(target);
            if (match.getScore() > score) {
                return match;
            }
        } catch (FindFailed findFailed) {
            Debug.error(findFailed.getMessage());
        }
        return null;
    }
}
