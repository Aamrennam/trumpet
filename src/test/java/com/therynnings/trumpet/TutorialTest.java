package com.therynnings.trumpet;

import org.sikuli.script.ImagePath;
import org.sikuli.script.Match;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TutorialTest extends BaseTest {
    @BeforeClass
    private void setSikuliSettings() {
        ImagePath.setBundlePath(TutorialTest.class.getClassLoader().getResource("tutorial").getPath());
    }

    @Test(priority = 1)
    public void testTutorial1() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep1", 60);
        System.out.println(match.getScore());
    }

    @Test(priority = 2)
    public void testTutorial2() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep2");
        System.out.println(match.getScore());
    }

    @Test(priority = 3)
    public void testTutorial3() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep3");
        System.out.println(match.getScore());
    }

    @Test(priority = 4)
    public void testTutorial4() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep4");
        System.out.println(match.getScore());
    }

    @Test(priority = 5)
    public void testTutorial5() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep5");
        System.out.println(match.getScore());
    }

    @Test(priority = 6)
    public void testTutorial6() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep6");
        System.out.println(match.getScore());
    }

    @Test(priority = 7)
    public void testTutorial7() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep7");
        System.out.println(match.getScore());
    }

    @Test(priority = 8)
    public void testTutorial8() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep8");
        System.out.println(match.getScore());
    }

    @Test(priority = 9)
    public void testTutorial9() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep9");
        System.out.println(match.getScore());
    }

    @Test(priority = 10)
    public void testTutorial10() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep10");
        System.out.println(match.getScore());
    }

    @Test(priority = 11)
    public void testTutorial11() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep11");
        System.out.println(match.getScore());
    }

    @Test(priority = 12)
    public void testTutorial12() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep12");
        System.out.println(match.getScore());
    }

    @Test(priority = 13)
    public void testTutorial13() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep13");
        System.out.println(match.getScore());
    }

    @Test(priority = 14)
    public void testTutorial14() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep14");
        System.out.println(match.getScore());
    }

    @Test(priority = 15)
    public void testTutorial15() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep15");
        System.out.println(match.getScore());
    }

    @Test(priority = 16)
    public void testTutorial16() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep16");
        System.out.println(match.getScore());
    }

    @Test(priority = 17)
    public void testTutorial17() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep17");
        System.out.println(match.getScore());
    }

    @Test(priority = 18)
    public void testTutorial18() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep18");
        System.out.println(match.getScore());
    }

    @Test(priority = 19)
    public void testTutorial19() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep19");
        System.out.println(match.getScore());
    }

    @Test(priority = 20)
    public void testTutorial20() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep20");
        System.out.println(match.getScore());
    }

    @Test(priority = 21)
    public void testTutorial21() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep21");
        System.out.println(match.getScore());
    }

    @Test(priority = 22)
    public void testTutorial22() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep22");
        System.out.println(match.getScore());
    }

    @Test(priority = 23)
    public void testTutorial23() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep23");
        System.out.println(match.getScore());
    }

    @Test(priority = 24)
    public void testTutorial24() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep24");
        System.out.println(match.getScore());
    }

    @Test(priority = 25)
    public void testTutorial25() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep25");
        System.out.println(match.getScore());
    }

    @Test(priority = 26)
    public void testTutorial26() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep26");
        System.out.println(match.getScore());
    }

    @Test(priority = 27)
    public void testTutorial27() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep27");
        System.out.println(match.getScore());
    }

    @Test(priority = 28)
    public void testTutorial28() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep28");
        System.out.println(match.getScore());
    }

    @Test(priority = 29)
    public void testTutorial29() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep29");
        System.out.println(match.getScore());
    }


    @Test(priority = 30)
    public void testTutorial30() throws InterruptedException {
        Match match = screen.waitClick("tutorialStep30");
        System.out.println(match.getScore());
    }
}
