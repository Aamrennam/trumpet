package com.therynnings.trumpet;

import org.sikuli.script.ImagePath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AccountLoginTest extends BaseTest {
    @BeforeClass
    private void setSikuliSettings() {
        //ImagePath.setBundlePath(TutorialTest.class.getClassLoader().getResource("accountlogin").getPath());
    }


    @Test(priority = 0)
    private void testEnterProfile() {
        screen.tap(100, 100);
    }

    @Test(priority = 1)
    private void testAccountButton() {
        screen.tap(190, 860);
    }

    @Test(priority = 2)
    private void testSwitchAccountButton() {
        screen.tap(550, 1260);
    }

    @Test(priority = 3)
    private void testChooseAccountType() {

    }
}
