package com.therynnings.trumpet;

import org.sikuli.basics.Debug;
import org.sikuli.script.FindFailed;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Match;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test(groups = "vip")
public class VIPTest extends BaseTest {

    @BeforeClass
    private void setSikuliSettings() {
        ImagePath.setBundlePath(TutorialTest.class.getClassLoader().getResource("vipmonsterattack").getPath());
    }

    @Test(dependsOnMethods = "testBaseTest")
    public void testNavigateMapView() {
        Match result;
        try {
            result = screen.find("enterMapButton", 0.9);
            if (result != null) {
                screen.aTap(result);
            }
        } catch (FindFailed ex) {
            ex.printStackTrace();
        }
    }

    @Test(dependsOnMethods = { "testNavigateMapView" })
    public void testVIPActive() {
        Match result = null;
        try {
            screen.tap(1014, 1506);
            result = screen.find("checkVIPActive");
            if (result != null) {
                Debug.error("Please activate VIP in game.");
                Assert.fail("VIP is not active");
            }
        } catch (FindFailed findFailed) {
            Debug.info("VIP is active.");
            Assert.assertNull(result);
        }
    }
}
