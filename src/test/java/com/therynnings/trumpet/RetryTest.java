package com.therynnings.trumpet;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryTest implements IRetryAnalyzer {

    private int count = 0;
    private static int maxTry = 5;

    @Override
    public boolean retry(ITestResult iTestResult) {
        if (count < maxTry) {
            // Don't skip if retry success
            iTestResult.setStatus(ITestResult.SKIP);
            iTestResult.getTestContext().getSkippedTests().removeResult(iTestResult.getMethod());
            count++;
            return true;
        }
        return false;
    }
}
