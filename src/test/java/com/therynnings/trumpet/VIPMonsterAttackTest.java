package com.therynnings.trumpet;

import org.sikuli.basics.Debug;
import org.sikuli.script.FindFailed;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Match;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


@Test(groups="monsterattack")
public class VIPMonsterAttackTest extends VIPTest {
    final String MONSTER_LEVEL = "25";

    @BeforeClass
    private void setSikuliSettings() {
        ImagePath.setBundlePath(TutorialTest.class.getClassLoader().getResource("vipmonsterattack").getPath());
    }

    @Test(dependsOnMethods = { "testVIPActive" })
    public void testFindMonster() throws InterruptedException {
        //Assert.assertNotNull(screen.waitClick("vipSearchMonster"));
        screen.tap(966, 1530);
        //Assert.assertNotNull(screen.waitClick("vipSearchLevel"));
        screen.tap(878, 1722);
        // Type monster level
        screen.type(MONSTER_LEVEL);

        //Assert.assertNotNull(screen.waitClick("vipSearchInputConfirm"));
        screen.tap(1026,1834);
        Assert.assertNotNull(screen.waitClick("vipSearchGo"));
    }

    @Test(dependsOnMethods = { "testFindMonster" }, retryAnalyzer = RetryTest.class)
    public void testPrepareAttackMonster() throws InterruptedException {
        // Wait for monster objects to load
        Thread.sleep(2500);
        // Press approximate monster location
        screen.tap(540, 870);
        Assert.assertNotNull(screen.waitClick("attackMonsterButton"));
    }

    @Test(priority = 1)
    public void testChooseArmyPresetAndStartAttack() {
        // Choose quick select army 1
        screen.tap(565, 355);
        Assert.assertNotNull(screen.waitClick("attackSetOutButton"));
    }
}