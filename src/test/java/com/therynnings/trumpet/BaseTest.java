package com.therynnings.trumpet;

import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Match;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.IOException;

@Test(groups = "init")
public class BaseTest {
    public Trumpet screen;

    @BeforeSuite
    public void setUp() {
        Settings.DebugLogs = true;
        Settings.ActionLogs = true;
        Settings.InfoLogs = true;
        ImagePath.setBundlePath(TutorialTest.class.getClassLoader().getResource("Images").getPath());
        screen = new Trumpet();

        try {
            Runtime.getRuntime().exec("adb shell monkey -p com.camelgames.superking -c android.intent.category.LAUNCHER 1");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testBaseTest() throws InterruptedException {
        Match result;
        try {
            //result = screen.find("enterMapButton", 0.6);
            result = screen.wait("enterCastleButton", 60);
            Assert.assertNotNull(result);
        } catch (FindFailed ex) {
            screen.aKey(12);
        }
    }

    @AfterSuite
    public void tearDown() {
        if (screen != null) {
            screen = null;
        }
    }
}
